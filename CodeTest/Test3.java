import java.util.ArrayList;
import java.util.List;

public class Test3 {
    public static void main(String[] args) {
        String[] words = {"names", "agus", "dika", "sukirman", "samsul", "dwiki", "tayo"};
        int x = 8;
        int a = 4;
        int b = 5;

        System.out.println(listWords(words, x));//The result should be [sukirman]
        System.out.println(listWords(words, a));//The result should be [agus, dika, tayo]
        System.out.println(listWords(words, b));//The result should be [names, dwiki]
    }

    static List<String> listWords(String[] words, int x) {
        List<String> newWords = new ArrayList<>();
        for (String string : words) {
            if (string.length() == x) {
                newWords.add(string);
            }
        }
        return newWords;
    }
}