import java.util.ArrayList;
import java.util.List;

public class Test2 {
    public static void main(String[] args) {
        int[] nums = {10,2,3,25,15,4,8,7,5};
        int x = 5;
        System.out.println(numbers(nums, x));//The result shoud be [2,3,4,8,7] because 10/2=5,  25/5=5, 15/3=5, 5/1=5 

        int[] nums2 = {100,500,1000,10000,79,85};
        int x2 = 100;
        System.out.println(numbers(nums2, x2));//The result should be [79,85] because 100/1=100,  500/5=100,  1000/10=100,  10000/100=100
    }

    static List<Integer> numbers(int[] nums, int x) {
        List<Integer> num1 = new ArrayList<>();
        List<Integer> num2 = new ArrayList<>();
        for (int i : nums) {
            for (int j = 1; j <= 100; j++) {
                if (i / j == x) {
                    num1.add(i);
                }
            }
            num2.add(i);
        }
        num2.removeAll(num1);

        return num2;
    }
}
