import java.util.Arrays;

public class Test1 {

    public static void main(String[] args) {

        int[] nums = {2,8,5,6,1,13,13,7,10,12};

        System.out.println(func1(nums));//The result is 13
        System.out.println(func2(nums));//The result is 13

    }

    static int func1(int[] nums) {
        Arrays.sort(nums);
        int result = nums[nums.length - 1];
        return result;
    }

    static int func2(int[] nums) {
        int result = 0;
        for (int i : nums) {
            if (result <= i) {
                result = i;
            }
        }
        return result;
    }
    
}
