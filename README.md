# Requirement
- Installed JDK on computer (https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK_Howto.html)
- IDE (VS Code, Intellij idea, Eclipse or other IDE)

# How to running code
- open youre console or terminal then paste "git clone https://gitlab.com/heru-ardiansah/code-test.git"
- or download this repository as zip then extract to any folders


# On VS code
- Open extracted folder
- open terminal
- javac "Filename.java", for example(javac Test1.java)
- java "Filename" without extension, for example(java Test1) 
